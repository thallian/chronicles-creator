# Chronicles Creator

A small tool I wrote to create a simple chronicle from some structured data.
The base idea was to create a consistent layout for some existing stuff.

## Prerequisites

- python >= 3.3
- sqlalchemy >= 0.9
- for the pdf creation: LaTeX environment (*lualatex* or *pdflatex* has to be in your path)
- an sqlite database with the data (take a look at *sql/schema.sql*)

All is licensed under the [MPL 2.0](https://www.mozilla.org/MPL/2.0/).

Install by running `python setup.py install` and then use it as `chronicle-creator`
