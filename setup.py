#!/usr/bin/env python

from distutils.core import setup

setup(name='chronicle-creator',
    version='0.0.1',
    description='Creates a timeline of some structured data.',
    author='Sebastian Hugentobler',
    author_email='shugentobler@vanwa.ch',
    url='https://code.vanwa.ch/',
    license='MPL 2.0',
    packages=['chronicle_creator'],
    scripts=['bin/chronicle-creator'],
    package_data={
        'chronicle_creator': ['templates/*.j2']
    }
)
