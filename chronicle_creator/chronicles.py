# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
# (c) 2015 by Sebastian Hugentobler <shugentoblervanwa.ch>

# -*- coding: utf-8 -*-

import os
import shutil
import subprocess

from jinja2 import Environment, PackageLoader, Template
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from .orm import Event, Month

class PdfException(Exception):
    pass

def date_range(event, year_unit):
    datestring = "{0!s:~>4} {1}".format(event.year_start, year_unit)

    if event.day_start:
        datestring += " {0:02d}.".format(event.day_start)

    if event.month_start:
        datestring += " {0}".format(event.month_start.abbreviation)

    if event.year_end:
        datestring += " - {0} {1}".format(event.year_end, year_unit)

    return datestring

def source_pages(source_events):
    source_concat = {}

    for source_event in source_events:
        new = "{0}-{1},".format(source_event.page_start, source_event.page_end) if source_event.page_end else "{0},".format(source_event.page_start)
        source_concat[source_event.source.abbreviation] = source_concat[source_event.source.abbreviation] + new if source_event.source.abbreviation in source_concat else new

    for key in source_concat:
        source_concat[key] = source_concat[key][:-1]

    source_string = ''
    for key in source_concat:
        source_string += "{0}:{1} ".format(key, source_concat[key])

    return source_string[:-1]

def render_pdf(output):
    render_cmd = shutil.which('lualatex') if shutil.which('lualatex') is not None else shutil.which('pdflatex')
    if render_cmd is None:
        raise PdfException('No valid latex command was found')

    tmp_dir = './tmp'
    os.makedirs(tmp_dir, exist_ok=True)
    subprocess.call([render_cmd, '--output-directory', tmp_dir, output])

    filename = os.path.basename(output)
    filename_split_index = filename.rfind('.')
    if filename_split_index != -1:
        filename = filename[:filename_split_index]

    shutil.copy(os.path.join(tmp_dir, filename + '.pdf'), os.path.dirname(output))
    shutil.rmtree(tmp_dir)

def render(title, year_start, year_end, database, output, make_pdf, header_year, header_event, header_sources, year_unit):
    engine = create_engine('sqlite:///' + database, echo=False)
    Session = sessionmaker(bind=engine)

    session = Session()
    events = session.query(Event) \
        .filter(Event.year_start >= year_start) \
        .filter(Event.year_start <= year_end) \
        .all()

    env = Environment(loader=PackageLoader('chronicle_creator', 'templates'))
    env.block_start_string = '((*'
    env.block_end_string = '*))'
    env.variable_start_string = '((('
    env.variable_end_string = ')))'
    env.comment_start_string = '((='
    env.comment_end_string = '=))'
    env.filters['date_range'] = date_range
    env.filters['source_pages'] = source_pages

    template = env.get_template('chronicles.j2')

    global_year_unit = year_unit

    meta = {}
    meta['title'] = title
    meta['year_start'] = year_start
    meta['year_end'] = year_end
    meta['header_year'] = header_year
    meta['header_event'] = header_event
    meta['header_sources'] = header_sources
    meta['year_unit'] = year_unit



    rendered_chronicles = template.render(meta=meta, events=events)

    os.makedirs(os.path.dirname(output), exist_ok=True)

    with open(output, 'w') as f:
        f.write(rendered_chronicles)

    if make_pdf:
        render_pdf(output)
