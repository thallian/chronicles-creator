# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
# (c) 2015 by Sebastian Hugentobler <shugentoblervanwa.ch>

# -*- coding: utf-8 -*-

from sqlalchemy import Column, ForeignKey, Integer, String, Table
from sqlalchemy import create_engine
from sqlalchemy.orm import relationship, backref
from sqlalchemy.ext.associationproxy import association_proxy
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

class Month(Base):
    __tablename__ = 'months'
    __table_args__ = {'sqlite_autoincrement': True}

    id = Column(Integer, primary_key=True)
    name = Column(String)
    abbreviation = Column(String)

    def __repr__(self):
        return "<Month(%s)>" % (self.name)

class Source(Base):
    __tablename__ = 'sources'
    __table_args__ = {'sqlite_autoincrement': True}

    id = Column(Integer, primary_key=True)
    name = Column(String)
    abbreviation = Column(String)

    def __repr__(self):
        return "<Source(%s)>" % (self.name)

class EventSource(Base):
    __tablename__ = 'event_sources'
    __table_args__ = {'sqlite_autoincrement': True}

    id = Column(Integer, primary_key=True)

    event_id = Column(Integer, ForeignKey('events.id'))
    event = relationship('Event')

    source_id = Column(Integer, ForeignKey('sources.id'))
    source = relationship('Source')

    page_start = Column(Integer)
    page_end = Column(Integer)

class Event(Base):
    __tablename__ = 'events'
    __table_args__ = {'sqlite_autoincrement': True}

    id = Column(Integer, primary_key=True)
    text = Column(String)
    year_start = Column(Integer)
    year_end = Column(Integer)

    month_start_id = Column(Integer, ForeignKey('months.id'))
    month_start = relationship('Month', foreign_keys='Event.month_start_id')

    month_end_id = Column(Integer, ForeignKey('months.id'))
    month_end = relationship('Month', foreign_keys='Event.month_end_id')

    day_start = Column(Integer)
    day_end = Column(Integer)

    sources = relationship('EventSource', backref='events', order_by="EventSource.page_start")

    def __repr__(self):
        return "<Event(%s)>" % (self.text)
