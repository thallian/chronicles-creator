--
-- Text encoding used: UTF-8
--
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Table: months
CREATE TABLE months (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT NOT NULL, abbreviation TEXT NOT NULL)

-- Table: events
CREATE TABLE events (id INTEGER PRIMARY KEY AUTOINCREMENT, text TEXT NOT NULL, year_start INTEGER NOT NULL, year_end INTEGER, month_start_id INTEGER REFERENCES months (id), month_end_id INTEGER REFERENCES months (id), day_start INTEGER, day_end INTEGER)

-- Table: sources
CREATE TABLE sources (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT NOT NULL, abbreviation TEXT NOT NULL)

-- Table: event_sources
CREATE TABLE event_sources (event_id INTEGER REFERENCES events (id) NOT NULL, source_id INTEGER REFERENCES sources (id) NOT NULL, page_start INTEGER, page_end INTEGER, id INTEGER PRIMARY KEY AUTOINCREMENT)

COMMIT TRANSACTION;
